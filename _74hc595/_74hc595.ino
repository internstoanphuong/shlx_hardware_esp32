int data = 4;   //pin 14 on the 75HC595
int latch = 0;  //pin 12 on the 75HC595
int clk = 2; //pin 11 on the 75HC595

#define bai1 led1[0]
#define bai2 led1[1]
#define bai3 led1[2]
#define bai4 led1[3]
#define cam_bien_1 led2[0]
#define cam_bien_2 led2[1]
#define tx_led led2[2]
#define rx_led led2[3]
#define number 3
#define Pin number * 8
boolean registers[Pin];

void setup() {
  Serial.begin (115200);
  pinMode(data, OUTPUT);
  pinMode(latch, OUTPUT);
  pinMode(clk, OUTPUT);

}

unsigned int digital[]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90};
unsigned int led[]={0x10,0x20,0x40,0x80,0x01,0x02,0x04,0x08};
void push(unsigned long d1,unsigned long d2,unsigned long d3)//ham truyen 595
{
unsigned long tg,i;
digitalWrite(latch, LOW);
for(i=0;i<8;i++)
{
tg=d1;
tg=tg&0x80;
digitalWrite (clk,0);
if(tg==0x80)
digitalWrite (data,1);
else
digitalWrite (data,0);

digitalWrite (clk,1);
d1=d1<<1;
}
for(i=0;i<8;i++)
{
tg=d2;
tg=tg&0x80;
digitalWrite (clk,0);
if(tg==0x80)
digitalWrite (data,1);
else
digitalWrite (data,0);

digitalWrite (clk,1);
d2=d2<<1;
}
for(i=0;i<8;i++)
{
tg=d3;
tg=tg&0x80;
digitalWrite (clk,0);
if(tg==0x80)
digitalWrite (data,1);
else
digitalWrite (data,0);

digitalWrite (clk,1);
d3=d3<<1;
}
  digitalWrite(latch, HIGH);
}
unsigned int fameled1(int leddon1[])
{
  unsigned int valueled_1=0;
  int run;
  unsigned int led1[]={0x10,0x20,0x40,0x80};
      for (run=0;run<4;run++)
      {
        Serial.print(leddon1[run]);
        Serial.println ();
        if (leddon1[run]==1) valueled_1=valueled_1+led1[run]; 
   }
   return valueled_1;
  }
unsigned int fameled2(int leddon2[])
{
  unsigned int valueled_2=0;
  int run;
  unsigned int led2[]={0x01,0x02,0x04,0x08};
      for (run=0;run<4;run++)
      {
        if (leddon2[run]==1) valueled_2=valueled_2+led2[run]; 
   }
   return valueled_2;
  }

void printdigital(int seg0,int seg1 ,int seg2,int seg3,unsigned int led1,unsigned int led2)
{ int i;
for (i=0;i<20;i++)
{
 push(led2,0x01+led1,digital[seg0]);
 delay(5);
 push(led2,0x02+led1,digital[seg1]);
 delay (5);
 push(led2,0x04+led1,digital[seg2]);
 delay (5);
 push(led2,0x08+led1,digital[seg3]);
 delay (5);
}
  }
void loop() {
  int i,dem;
  unsigned int ledhienthi1;
    unsigned int ledhienthi2;
 int led1[]={0,0,0,0};
 int led2[]={0,0,0,0};
 cam_bien_1=HIGH;
 bai1=HIGH;
 printdigital(6,7,2,3,fameled1(led1),fameled2(led2));
}

