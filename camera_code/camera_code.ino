#include <HardwareSerial.h>
#include "SD_MMC.h"
#include "FS.h"
HardwareSerial camera(2);
#include "httpclient.h"
unsigned int picTotalLen = 0;  // picture length
int picNameNum = 0;
File img ;
void setup() {

  Serial.begin (115200);
  camera.begin (115200);
  if (!SD_MMC.begin()) {
    Serial.println("Card Mount Failed");
    return;

  }
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  WiFi.config(ip, gateway, subnet);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
     }
  Serial.println("");
  Serial.print("WiFi connected,");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi); 
  reset();
   
}
void reset ()
{
  byte resetcmd[] = {0x56, 0x00, 0x26 , 0x00};
  camera.write(resetcmd, 4);
  delay (106);
  while (camera.available())
  {
    camera.read();
  }
}
uint8_t cmdSend[] = {0x56, 0x00 , 0x54 , 0x01 , 0x11};
int run;
void sendCmd(char cmd[] , int cmd_len)
{
  for (char i = 0; i < cmd_len; i++)camera.write(cmd[i]);
}

void takepicture()
{
  uint8_t capture[] = {0x56, 0x00, 0x36, 0x01, 0x00};
  camera.write (capture, 5);
  delay(106);
  while (camera.available() > 0) {
    Serial.print(camera.read(),HEX);
  }
  Serial.println ();
}
void setsize()
{
  uint8_t  size[] = { 0x56 , 0x00 , 0x54, 0x01 , 0x00};
  camera.write (size, 5) ;
    delay(106);
    while (camera.available() > 0) {
    Serial.print(camera.read(),HEX);
  }
  Serial.println ();
}
void setfile()
{
  uint8_t  size[] = { 0x56 , 0x00 , 0x34, 0x01 , 0x00};
  camera.write (size, 5) ;
    delay (106);
    byte bien[]={};
    while (camera.available() > 0) {
      for (int i=0;i<9;i++)
      {
      bien[i]=camera.read();
    }
  }
  //for (run=0;run<9;run++){
  {
    Serial.write(bien,9);
}
}
unsigned int filesize()
{
  uint8_t filesize[] = {0x56, 0x00 , 0x34 , 0x01, 0x00};
  camera.write (filesize, 5);
  byte inChar[] = {};
  delay (106);
  while (camera.available()) {
    // get the new byte:
    for (run = 0; run < 9; run++) {
      inChar[run] = camera.read();
     // Serial.println (inChar[run],HEX);
    }
  }
  return inChar[7] * 256 + inChar[8];
}

void getdata(long lenf)
{
  long lenfile = lenf;
  byte readfram[] = { 0x56, 0x00, 0x32, 0x0C, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x0A };
  int count = lenfile / 192;
  int tail = lenfile % 192;
  char picName[] = "/shlx_img000000.jpg";
  picName[12] = picNameNum / 10 + '0';
  picName[13] = picNameNum % 10 + '0';
  //Serial.println(count);
  //Serial.println(tail);
  File myFile = SD_MMC.open(picName, FILE_WRITE);
  if (!myFile) {
    Serial.println("myFile open fail...");
  }
  else {

  } byte data[] = {};
  long addr = 0;
  for (int i = 0; i < (count); i++) {    //get and save count*PIC_BUF_LEN data
    camera.write(readfram, 16);
   delay (30);
    while (camera.available()) {
      readCamSaveToFile(myFile, 192);
    }
    addr = addr + 192;
    readfram[8] = addr >> 8;
    readfram[9] = addr & 0x00FF;
    //Serial.println (readfram[8],HEX);
    //Serial.println (readfram[9],HEX);
        
  }

  readfram[12] = tail >> 8;
  readfram[13] = tail & 0x00FF;
  //get rest part of the pic data
  camera.write(readfram, 16);
  delay (30);
  while (camera.available()) {
  readCamSaveToFile(myFile, tail);
  }
  myFile.close();
}

void readCamSaveToFile(File &myFile, int toBeReadLen)
{

  char readLen = 0;
  for (char run = 0; run < 5; run ++) { //read the signal that tells us that it's ready to send data
    camera.read();
  }
  while (readLen < (toBeReadLen)) { //read and store the JPG data that starts with 0xFF 0xDB
    myFile.write((byte)camera.read());
    readLen++;
  }
  for (char run = 0; run < 5; run ++) { //read the signal of sussessful sending
    camera.read();
  }
}
void stopcap()
{
  uint8_t stopcap[] = {0x56 , 0x00 , 0x36 , 0x01 , 0x03};
  camera.write(stopcap, 5);
  delay (106);
  while (camera.available()) {
    camera.read();
  }
}
void listDir(fs::FS &fs, const char * dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}


void loop() {
  long i = 0;
  long t = 0;
  picNameNum++;
  char picName[] = "/shlx_img000000.jpg";
  picName[12] = picNameNum / 10 + '0';
  picName[13] = picNameNum % 10 + '0';
  stopcap();
   takepicture();
  setsize();
  setfile(); 
  getdata(filesize());
  sendfile(picName);
  listDir(SD_MMC, "/", 0);
  delay (1000);
}
