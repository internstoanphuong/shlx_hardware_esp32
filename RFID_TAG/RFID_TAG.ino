#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN 5
#define RST_PIN 36
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
#define LED 25
/*
 SS    = 5;
 MOSI  = 23;
 MISO  = 19;
 SCK   = 18;
*/
void setup() 
{
  Serial.begin(115200);   // Initiate a serial communication
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  Serial.println("cong nghe toan phuong/ quet the ID");
  Serial.println();
  pinMode(LED ,OUTPUT);
  digitalWrite(LED,LOW);  
}
void loop() 
{
 
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Message : ");
  content.toUpperCase();
  if (content.substring(1) == "F1 70 32 3B" or content.substring(1)=="64 D2 15 B8") 
  {
    Serial.println("Authorized access");
    Serial.println();
    digitalWrite(LED,!digitalRead(LED));
    delay(3000);
  }
 
 else   
 {
    Serial.println(" Access denied");
    delay(1000);
  }
}
