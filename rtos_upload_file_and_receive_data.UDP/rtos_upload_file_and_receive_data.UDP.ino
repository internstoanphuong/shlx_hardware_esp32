/* structure that hold data*/
#include <WiFi.h>
#include <WiFiClient.h>
#include "SD_MMC.h"
#include <WiFiUDP.h>
#include <WiFiUdp.h>
#include <SPI.h>
#include "FS.h"
File webFile;
char servername[] = "192.168.100.36";
const char* ssid = "WIFIT2"; //Ten Wifi
const char* pw = "hru12345680"; //Pass Wifi
char packetBuffer[255];
WiFiClient client;
unsigned long laikas;
char  replyBuffer[] = "hung";  //Du lieu gui len server
char IPserver[] = "192.168.100.41";
unsigned int UDPPort = 80;
IPAddress IPa(192, 168, 100, 160);
IPAddress gateway(192, 168, 100, 3);
IPAddress subnet(255, 255, 255, 0);
WiFiUDP Udp;
long error=0;
byte eRcv()
{
  byte respCode;
  byte thisByte;
  while(!client.available()) delay(1);
  respCode = client.peek();
  while(client.available())
  {  
    thisByte = client.read();    
    Serial.write(thisByte);
  }
  return 1;
}


/* this variable hold queue handle */
xQueueHandle xQueue;
TaskHandle_t xTask1;
TaskHandle_t xTask2;
void setup() {

  Serial.begin(112500);
  WiFi.begin(ssid, pw);
   SD_MMC.begin();
  IPAddress ip = WiFi.localIP();
  WiFi.config(IPa, gateway, subnet);
  Serial.println(ip);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
  Udp.begin(UDPPort);
  Udp.beginPacket(IPserver, UDPPort);
  char ipBuffer[255];
  /* create the queue which size can contains 5 elements of Data */
               /* pin task to core 1 */   
  xTaskCreatePinnedToCore(
      sendTask,           /* Task function. */
      "sendTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,                /* Task handle to keep track of created task */
      1);                    /* pin task to core 0 */
  
}

void loop() {
   xTaskCreatePinnedToCore(
      receiveTask,           /* Task function. */
      "receiveTask",        /* name of task. */
      10000,                    /* Stack size of task */
      NULL,                     /* parameter of the task */
      1,                        /* priority of the task */
      NULL,            /* Task handle to keep track of created task */
      0);  
      delay(1000);

}
void sendTask( void * parameter )
 { laikas = millis();
  size_t len=0;
    laikas = millis() - laikas;
 // Serial.println(laikas);
  laikas = millis();
  //SIUNTIMAS HTTTP
    if (client.connect(servername, 80)) {
      client.setNoDelay(true);
      Serial.println("Connected to Server");
      webFile = SD_MMC.open("/12345678.jpg");
      size_t len1=webFile.size();
      len=webFile.size();
      static uint8_t buf[512];
  
      laikas = millis();
      // Make a HTTP request:
      client.println("POST /post.php HTTP/1.1");
      client.println("Host: 192.168.100.36"); //change it
      client.println("Connection: close");
      //client.println("Cache-Control: max-age=0");
      client.println("Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryjg2qVIUS8teOAbN322");
      client.print("Content-Length: "); //file size  //change it
      client.println((String) (len1+196));
      client.println();
      client.println("------WebKitFormBoundaryjg2qVIUS8teOAbN322");
      client.println("Content-Disposition: form-data; name=\"file\"; filename=\"anh.jpg\""); //change it
      client.println("Content-Type: application/octet-stream");
      client.println();
    
      while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            webFile.read(buf, toRead);
            client.write(buf,toRead);
            len -= toRead;
       
        }
      webFile.close();
      client.println(); //file end
      client.println("------WebKitFormBoundaryjg2qVIUS8teOAbN322--");
      client.println();}
  //    Serial.println(len1);
     // Serial.println(len);}
  laikas = millis() - laikas;
  //prints time since sending started
  Serial.println(laikas);
  if(!eRcv()){
  client.stop();
  SD_MMC.end();
  }
  delay(1000);
  vTaskDelete( NULL );
  
}

void receiveTask( void * parameter )
{ 
  int packetSize = Udp.parsePacket();
  if (packetSize) 
  {
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(":");
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) 
    {
      packetBuffer[len] = 0;
    }
    Serial.println(packetBuffer); 
  }
  else error +=1;
// Gui du lieu len server:
  Udp.beginPacket(IPserver, UDPPort);
  Udp.printf(replyBuffer);
  Udp.endPacket();
   vTaskDelete( NULL );
 }
