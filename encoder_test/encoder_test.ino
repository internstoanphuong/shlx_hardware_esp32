#define ENC_A 12
#define ENC_B 14

// initially was going to hand pull the required bits from esp32 gpio via
// the 32bit functions gpio_input_get() and gpio_input_get_high() and test
// something like gpio_input_get() & (1<<11 | 1<< 21) == (1<<11 | 1<<21)
// but looking for the docs on the functions I found this line in gpio.h

//#define GPIO_INPUT_GET(gpio_no)     ((gpio_no < 32)? ((gpio_input_get()>>gpio_no)&BIT0) : ((gpio_input_get_high()>>(gpio_no - 32))&BIT0))

// this will involve two calls to the function which is not okay really,
// as the pins may have changed value between calls potentially. One should
// suffice, maybe at the expense of a second variable or additional shifts.
#include <rom/gpio.h>

// static 
  uint8_t enc_states[] = {0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0};
  static uint8_t old_AB = 0;
  static uint32_t curval = 0;
  static uint32_t curtmpA = 0;
  static uint32_t curtmpB = 0;
  static uint8_t counter = 0;      //this variable will be changed by encoder input
 long tmpdata;
 long dem=0;
void setup()
{
  /* Setup encoder pins as inputs */
  pinMode(ENC_A, INPUT_PULLUP);
  digitalWrite(ENC_A, HIGH);
  pinMode(ENC_B, INPUT_PULLUP);
  digitalWrite(ENC_B, HIGH);
  Serial.begin (115200);
      delay(10);
    delay(10);

  //Serial.println("Start");
}
 
void loop()
{
 
 /**/
  tmpdata = read_encoder();
  if( tmpdata ) {
    Serial.print("Counter value: ");
    Serial.println(dem, DEC);
    dem += tmpdata;
  }
    else{
   //   Serial.println("No Change");
      }
  //delay(3000);

} 
 
/* returns change in encoder state (-1,0,1) */
int8_t read_encoder()
{
  
  
  /**/
  old_AB <<= 2;                   //remember previous state
  //bit shift old_AB two positions to the left and store.

  curval = gpio_input_get();  // returns gpio pin status of pins - SEE DEFINE 
  //Serial.println("curval");
 // Serial.println(curval);
  //note to self: these curval bits are probably backwards...
 curtmpA = (curval & 1<< ENC_A ) >> ENC_A;
  //Serial.println("curtmp A");
  //Serial.println(curtmpA);

curtmpB=(curval & 1<< ENC_B ) >> (ENC_B - 1);
 //Serial.println ("curtmpB");
// Serial.println(curtmpB);
  old_AB |= ( ( curtmpA | curtmpB ) & 0x03 ); 
//  add current state and hopefully truncate to 8bit 
 
  return ( enc_states[( old_AB & 0x0f )]);
  // return the array item that matches the known possible encoder states

  // Thanks to kolban in the esp32 channel, who has a great book on everything iot,
  // for his initial help at my panic on the esp32 gpio access. long live IRC :)
}
