#include "i2s.h"
#include "SD_MMC.h"
#include "FS.h"
//GPIO 25,26,22
static const int i2s_num = 0; // i2s port number

static const i2s_config_t i2s_config = {
     .mode = I2S_MODE_MASTER | I2S_MODE_TX,
     .sample_rate = 44100,
     .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
     .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
     .communication_format = I2S_COMM_FORMAT_I2S ,
     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // high interrupt priority
     .dma_buf_count = 8,
     .dma_buf_len = 64
};

static const i2s_pin_config_t pin_config = {
    .bck_io_num = 26,
    .ws_io_num = 25,
    .data_out_num = 22,
    .data_in_num = I2S_PIN_NO_CHANGE
};
size_t len;
static uint8_t buf [128];

void setup() {
  // put your setup code here, to run once:
      Serial.begin (115200);
   if(!SD_MMC.begin()){
        Serial.println("Card Mount Failed");
        return;
    }

    i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);   //install and start i2s driver

    i2s_set_pin(I2S_NUM_0, &pin_config);

    i2s_set_sample_rates(I2S_NUM_0, 44100); //set sample rates
}

void loop() {
  // put your main code here, to run repeatedly:
  File file = SD_MMC.open("/audio.wav");
  if(!file){
        Serial.println("Failed to open file for reading");
        
    }
   len= file.size();
 while(len){
            size_t toRead = len;
            if(toRead > 128){
                toRead = 128;
            }
            file.read(buf, toRead);
            i2s_write_bytes(I2S_NUM_0,(const char *) buf, toRead,100);        
            len -= toRead;
        }
    

Serial.println();
  // put your main code here, to run repeatedly:
delay (1000);

}
